<!-- site-footer -->
<footer id="colophon" class="site-footer" role="contentinfo">
            <!-- layout-medium -->
            <div class="layout-medium">
                <!-- site-title-wrap -->
                <div class="site-title-wrap">

                    <h1 class="site-title">
                        <a href="index.html" rel="home">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </h1>
                    <!-- site-title -->
                    <p class="site-description">just living the life as it goes by</p>
                </div>
                <!-- site-title-wrap -->
                <!-- footer-social -->
                <div class="footer-social">
                    <div class="textwidget">
                        <a class="social-link" href="#"><i class='fab fa-github'></i></a>
                        <a class="social-link" href="#"><i class='fab fa-twitter'></i></a>
                    </div>
                </div>
            </div>
            <!-- layout-medium -->
            <!-- .site-info -->
            <div class="site-info">
                <!-- layout-medium -->
                <div class="layout-medium">
                    <div class="textwidget">crafted with <i class="fas fa-heart"></i> by Pixelwars</div>
                </div>
                <!-- layout-medium -->
            </div>
            <!-- .site-info -->
        </footer>
        <!-- site-footer -->
        </div>
    <!-- page -->
    <!-- SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        if (!window.jQuery) {
            document.write('<script src="js/jquery-3.1.1.min.js"><\/script>');
        }
    </script>
    <script src="js/jquery-migrate-3.0.0.min.js"></script>
    <script src="js/fastclick.js"></script>
    <script src="js/jquery.fitvids.js"></script>
    <script src="js/jquery.viewport.mini.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.fluidbox.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/selection-sharer.js"></script>
    <script src="js/socialstream.jquery.js"></script>
    <script src="js/jquery.collagePlus.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/shortcodes.js"></script>
    <script src="js/hasnah.js"></script>
</body>
<!-- InstanceEnd -->
</html>