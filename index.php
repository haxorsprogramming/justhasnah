<!DOCTYPE html>
<html lang="en" class="minimal-style is-menu-fixed is-always-fixed is-selection-shareable blog-animated header-light header-small" data-effect="slideUp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Impose - Responsive HTML5 Template">
    <meta name="keywords" content="personal, blog, html5">
    <meta name="author" content="Pixelwars">
    <title>Just Hasnah - Just a little web</title>
    <!-- FAV and TOUCH ICONS -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon" href="images/ico/apple-touch-icon.png" />
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Noto+Sans:400,400i,700,700i|Poppins:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/fontawesome/css/all.min.css">
    <!-- STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/fluidbox.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/selection-sharer.css">
    <link rel="stylesheet" type="text/css" href="css/rotate-words.css">
    <link rel="stylesheet" type="text/css" href="css/align.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/shortcodes.css">
    <link rel="stylesheet" type="text/css" href="css/768.css">
    <link rel="stylesheet" type="text/css" href="css/992.css">
    <!-- INITIAL SCRIPTS -->
    
    <script src="js/modernizr.min.js"></script>
</head>
<body onload="initFunc">
    <!-- page -->
    <div id="page" class="hfeed site">
        <!-- header -->
        <header id="masthead" class="site-header" role="banner">
            <!-- site-navigation -->
            <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
                <!-- layout-medium -->
                <div class="layout-medium">
                    <!-- site-title : image-logo -->
                    <h1 class="site-title">
                        <a href="index.html" rel="home">
                            <img src="img/logo.png" alt="logo">
                            <span class="screen-reader-text">Just Hasnah</span>
                        </a>
                    </h1>
                    <a class="menu-toggle"><span class="lines"></span></a>
                    <!-- nav-menu -->
                    <div class="nav-menu">
                        <ul>
                            <li><a href="index.php">Home</a>
                            <li><a href="me.php">About Me</a>
                            <li><a href="#!">My Works</a>
                            <li><a href="#!">Blog</a>
                            <li><a href="#!">Contact</a></li>
                        </ul>
                    </div>
                    <!-- nav-menu -->
                    <!-- search-container -->
                    <div class="search-container">

                        <div class="search-box" role="search">
                            <form method="get" class="search-form" action="#">
                                <label>Search Here
                                    <input type="search" id="search-field" placeholder="type and hit enter" name="s">
                                </label>
                                <input type="submit" class="search-submit" value="Search">
                            </form>
                        </div>
                    </div>
                    <!-- search-container -->
                    <!-- social-container -->
                    <div class="social-container">
                        <a class="social-link" href="#"><i class='fab fa-github'></i></a>
                        <a class="social-link" href="#"><i class='fab fa-twitter'></i></a>
                    </div>
                    <!-- social-container -->
                </div>
                <!-- layout-medium -->
            </nav>
            <!-- site-navigation -->
        </header>
        <!-- header -->
        <!-- site-main -->
        <div id="main" class="site-main">
           <div class="layout-medium" id='divUtama'>
                <div id="primary" class="content-area with-sidebar">
                    <!-- site-content -->
                    <div id="content" class="site-content" role="main">
                        <!-- BLOG LIST -->
                        <div class="blog-bold  blog-stream">
                            <!-- post -->
                            <article class="hentry has-post-thumbnail" v-for='bd in blog'>
                                <div class="post-thumbnail" style="background-image:url(img/pos/pos_1.jpg)">
                                    <!-- .entry-header -->
                                    <header class="entry-header">
                                        <!-- .entry-meta -->
                                        <div class="entry-meta">
                                            <span class="cat-links">
                                                <a href="#" title="View all posts in Life" rel="category tag">Life</a>
                                            </span>
                                        </div>
                                        <h3 class="entry-title" style="text-transform: capitalize;">
                                            <a href="#!">{{bd.title}}</a>
                                        </h3>
                                        <!-- .entry-meta -->
                                        <div class="entry-meta">
                                            <span class="entry-date">
                                                <time class="entry-date" datetime="2014-07-13T04:34:10+00:00">July 13, 2014</time>
                                            </span>
                                            <span class="comment-link">
                                                <a href="#comments">4 Comments</a>
                                            </span>
                                        </div>
                                        <!-- .entry-meta -->
                                        <p><a href="#!" class="more-link">View Post</a></p>
                                    </header>
                                    <!-- .entry-header -->
                                </div>
                            </article>
                        </div>
                        <!-- BLOG BOLD -->
                        <!-- post pagination -->
                        <nav class="post-pagination">
                            <ul class="pagination">
                                <li class="pagination-first"><a href="#!"> First </a></li>
                                <li class="pagination"><a href="#!" rel="prev"><i class="fas fa-angle-left"></i></a></li>
                                <li class="pagination-num"><a href="#!"> 1 </a></li>
                                <li class="pagination-num"><a href="#!"> 2 </a></li>
                                <li class="pagination-num"><a href="#!"> 3 </a></li>
                                <li class="pagination"><a href="#!" rel="prev"><i class="fas fa-angle-right"></i></a></li>
                                <li class="pagination-last"><a href="#!"> Last </a> </li>
                            </ul>
                        </nav>
                        <!-- post pagination -->
                    </div>
                    <!-- site-content -->
                </div>
                <!-- primary -->
                <!-- #secondary -->
                <div id="secondary" class="widget-area sidebar" role="complementary">
                    <!-- widget : text -->
                    <aside class="widget widget_text">
                        <h3 class="widget-title">About Me</h3>
                        <div class="textwidget">
                            <p><img src="img/about-me.jpg" alt="avatar">
                            </p>
                            <p>Hello. I am {{nama}} ,,, Mom of NadhaMedia, project manager at <a href='#!'>Haxorsprogramming</a>, and the lazy writer.</p>
                        </div>
                    </aside>
                    <!-- widget : text -->
                    <!-- widget : text -->
                    <aside class="widget widget_text">
                        <h3 class="widget-title">Follow Me</h3>
                        <div class="textwidget">
                            <p>
                                <a class="social-link" href="#"><i class='fab fa-github'></i></a>
                                <a class="social-link" href="#"><i class='fab fa-twitter'></i></a>
                            </p>
                        </div>
                    </aside>
                    <!-- widget : text -->
                    <!-- widget :  MailChimp for WordPress Plugin -->
                    <aside class="widget widget_mc4wp_widget">
                        <h3 class="widget-title">Subscribe To Newsletter</h3>
                        <div class="form mc4wp-form">
                            <form method="post">
                                <p>
                                    <label>Email address: </label>
                                    <input type="email" name="EMAIL" placeholder="Your email address" required="">
                                </p>
                                <p>
                                    <input type="submit" value="Sign up">
                                </p>
                            </form>
                        </div>
                    </aside>
                    <!-- widget :  MailChimp for WordPress Plugin -->
                    <!-- widget : popular-posts -->
                    <!-- styles for plugin : https://wordpress.org/plugins/top-10 -->
                    <aside class="widget widget_widget_tptn_pop">
                        <h3 class="widget-title">Trending Posts</h3>
                        <div class="tptn_posts tptn_posts_widget">
                            <ul>

                                <li>
                                    <a href="#" class="tptn_link">
                                        <img src="http://themes.pixelwars.org/impose-html/images/blog/p2.jpg" alt="post-image" class="tptn_thumb">
                                    </a>
                                    <span class="tptn_after_thumb">
                                        <a href="#" class="tptn_link"><span class="tptn_title">Feel The Wind</span></a>
                                        <!--<span class="tptn_author"> by <a href="#">Johnny Doe</a></span>-->
                                        <span class="tptn_date"> September 3, 2014</span>
                                    </span>
                                </li>

                            </ul>
                        </div>
                    </aside>
                    <!-- widget : popular-posts -->
                    <!-- widget : categories -->
                    <aside class="widget widget_categories">
                        <h3 class="widget-title">Categories</h3>
                        <ul>
                            <li class="cat-item"><a href="#" title="View all posts filed under Nature">Nature</a></li>
                            <li class="cat-item"><a href="#" title="View all posts filed under Life">Life</a></li>
                            <li class="cat-item"><a href="#" title="View all posts filed under Adventure">Adventure</a>
                            </li>
                            <li class="cat-item"><a href="#" title="View all posts filed under Freebies">Freebies</a>
                            </li>
                        </ul>
                    </aside>
                    <!-- widget : categories -->


                    <!-- widget : text -->
                    <aside class="widget widget_text">
                        <!--<h3 class="widget-title">BANNER</h3>-->
                        <div class="textwidget">
                            <a href="#"><img src="img/banner.jpg" alt="banner"></a>
                        </div>
                    </aside>
                </div>
                <!-- #secondary -->
            </div>
            <!-- layout -->
        </div>
        <!-- site-main -->
        <?php 
            include('footer.php');
        ?>