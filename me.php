<!DOCTYPE html>
<html lang="en" class="minimal-style is-menu-fixed is-always-fixed is-selection-shareable blog-animated header-light header-small" data-effect="slideUp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Impose - Responsive HTML5 Template">
    <meta name="keywords" content="personal, blog, html5">
    <meta name="author" content="Pixelwars">
    <title>Just Hasnah - Just a little web</title>
    <!-- FAV and TOUCH ICONS -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon" href="images/ico/apple-touch-icon.png" />
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Noto+Sans:400,400i,700,700i|Poppins:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://demo.getstisla.com/assets/modules/fontawesome/css/all.min.css">
    <!-- STYLES -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="css/fluidbox.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/selection-sharer.css">
    <link rel="stylesheet" type="text/css" href="css/rotate-words.css">
    <link rel="stylesheet" type="text/css" href="css/align.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/shortcodes.css">
    <link rel="stylesheet" type="text/css" href="css/768.css">
    <link rel="stylesheet" type="text/css" href="css/992.css">
    <!-- INITIAL SCRIPTS -->
    
    <script src="js/modernizr.min.js"></script>
</head>
<body onload="initFunc">
    <!-- page -->
    <div id="page" class="hfeed site">
        <!-- header -->
        <header id="masthead" class="site-header" role="banner">
            <!-- site-navigation -->
            <nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
                <!-- layout-medium -->
                <div class="layout-medium">
                    <!-- site-title : image-logo -->
                    <h1 class="site-title">
                        <a href="index.html" rel="home">
                            <img src="img/logo.png" alt="logo">
                            <span class="screen-reader-text">Just Hasnah</span>
                        </a>
                    </h1>
                    <a class="menu-toggle"><span class="lines"></span></a>
                    <!-- nav-menu -->
                    <div class="nav-menu">
                        <ul>
                            <li><a href="index.php">Home</a>
                            <li><a href="me.php">About Me</a>
                            <li><a href="#!">My Works</a>
                            <li><a href="#!">Blog</a>
                            <li><a href="#!">Contact</a></li>
                        </ul>
                    </div>
                    <!-- nav-menu -->
                    <!-- search-container -->
                    <div class="search-container">

                        <div class="search-box" role="search">
                            <form method="get" class="search-form" action="#">
                                <label>Search Here
                                    <input type="search" id="search-field" placeholder="type and hit enter" name="s">
                                </label>
                                <input type="submit" class="search-submit" value="Search">
                            </form>
                        </div>
                    </div>
                    <!-- search-container -->
                    <!-- social-container -->
                    <div class="social-container">
                        <a class="social-link" href="#"><i class='fab fa-github'></i></a>
                        <a class="social-link" href="#"><i class='fab fa-twitter'></i></a>
                    </div>
                    <!-- social-container -->
                </div>
                <!-- layout-medium -->
            </nav>
            <!-- site-navigation -->
        </header>
        <!-- header -->

 <!-- site-main -->
 <div id="main" class="site-main">
     <div class="layout-medium"> 
            <div id="primary" class="content-area">
                <!-- site-content -->
                <div id="content" class="site-content" role="main">
                     <!-- .hentry -->
                    <article class="hentry page">
                        
                                                    
                        <header class="entry-header">
                            <h1 class="entry-title">Curriculum Vitae</h1>
                        </header>
                        
                        
                        <!-- .entry-content -->
                        <div class="entry-content">
                        
                            <!-- row -->
                            <div class="row">
        
        
                                <!-- col 6/12 -->
                                <div class="col-sm-7">
                                    
                                    
                                    <!-- TIMELINE -->
                                    <div class="timeline">
                                        
                                        <!-- event -->
                                        <div class="event">
                                            <h2>WORK HISTORY</h2>
                                            <i class="pw-icon-bookmark-empty"></i>
                                        </div>
                                        <!-- event -->
                                        
                                        <!-- event -->
                                        <div class="event current">
                                            <h6>Dec 2013 - Current</h6>
                                            <h4>Front End Web Developer</h4>
                                            <h5>Pixelwars Inc.</h5>
                                            <p>I currently work for Pixelwars creative studio. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.</p>
                                        </div>
                                        <!-- event -->
                        
                                        <!-- event -->
                                        <div class="event">
                                            <h6>Jun 2012 - Dec 2013</h6>
                                            <h4>Web Developer</h4>
                                            <h5>Google Inc.</h5>
                                            <p>I worked as a Web Developer at Google for 3 years. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.</p>
                                        </div>
                                        <!-- event -->
                                        
                                        <!-- event -->            
                                        <div class="event">
                                            <h6>2006</span>
                                            <h4>Exclusive Author</h4>
                                            <h5>Envato Inc.</h5>
                                            <p>I am an Elite Author at Envato. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.</p>
                                        </div>
                                        <!-- event -->
                                        
                                        <!-- event -->                                            
                                        <div class="event">
                                            <h2>EDUCATION</h2>
                                            <i class="pw-icon-graduation-cap"></i>
                                        </div>
                                        <!-- event -->
                                        
                                        <!-- event -->            
                                        <div class="event">
                                            <h6>2002</h6>
                                            <h4>Atom Science</h4>
                                            <h5>Stanford University</h5>
                                            <p>I studied atomic stuff at Stanford University. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.</p>
                                        </div>
                                        <!-- event -->
                                        
                                        <!-- event -->
                                        <div class="event">
                                            <h6>2010</h6>
                                            <h4>Software Engineering</h4>
                                            <h5>Harvard University</h5>
                                            <p>I got my Master Degree at Harvard University. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.</p>
                                        </div>
                                        <!-- event -->
                                        
                                        <!-- event -->           
                                        <div class="event">
                                            <h6>2006</h6>
                                            <h4>Computer Science</h4>
                                            <h5>MIT</h5>
                                            <p>I studied Computer Science at MIT. I create usable web interfaces, front end coding stuff and almost anything. But i love what i do.</p>
                                        </div>
                                        <!-- event -->
                                        
                                    </div>
                                    <!-- TIMELINE -->
                                    
                                    <p><a href="#" class="button"><i class="pw-icon-linkedin-squared"></i>My LinkedIn Profile</a></p>
                                    
                                </div>
                                <!-- col 6/12 -->
        
                                <!-- col 6/12 -->
                                <div class="col-sm-5">
                                    
                                    <h3>DEVELOPER SKILLS</h3>
                                        
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>HTML5</h4>
                                        <div class="bar" data-percent="80">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
    
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>CSS3</h4>
                                        <div class="bar" data-percent="95">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
    
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>jQuery</h4>
                                        <div class="bar" data-percent="80">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
    
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>Wordpress</h4>
                                        <div class="bar" data-percent="60">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
                                    <h3>DESIGNER SKILLS</h3>
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>Fireworks</h4>
                                        <div class="bar" data-percent="90">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
    
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>Photoshop</h4>
                                        <div class="bar" data-percent="70">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
    
                                    <!-- .skill-unit -->
                                    <div class="skill-unit">
                                        <h4>Illustrator</h4>
                                        <div class="bar" data-percent="50">
                                            <div class="progress"></div>
                                        </div>
                                    </div>
                                    <!-- .skill-unit -->
                                     <h3>TESTIMONIALS</h3>
        
                                    <!-- Testimonial -->
                                    <div class="testo">
                                        <img src="images/site/testo-01.jpg" alt="someone">
                                        <h4>Vincent Wood
                                            <span>CEO / Gravity Inc.</span>
                                        </h4>
                                        <p>He is a great and hardworking guy. I am so proud of i have him as my asistant. He helped me so much. Also i am so proud of i have him as my asistant. He helped me so much.</p>
    
                                    </div>
                                    <!-- Testimonial -->
    
                                    <!-- Testimonial -->
                                    <div class="testo">
                                        <img src="images/site/testo-02.jpg" alt="someone">
    
                                        <h4>Gary Morgan
                                            <span>Chemist / Freelancer</span>
                                        </h4>
                                        <p>He was a great co-worker and a friend. I would't be where i am without his support.</p>
    
                                    </div>
                                    <!-- Testimonial -->
    
                                    <!-- Testimonial -->
                                    <div class="testo">
                                        <img src="images/site/testo-03.jpg" alt="someone">
    
                                        <h4>Jason Wilson
                                            <span>Lab Geek / Miami Metro</span>
                                        </h4>
                                        <p>He is ok. I don't really know him. He looks nice.</p>
    
                                    </div>
                                    <!-- Testimonial -->
                                </div>
                                <!-- col 6/12 -->
                            </div>
                            <!-- row -->
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .hentry -->
                <!-- InstanceEndEditable -->
                    
                </div>
                <!-- site-content -->
            </div>
            <!-- primary -->
        </div>
        <!-- layout -->
    </div>
    <!-- site-main -->
    <?php 
        include('footer.php');
    ?>
    